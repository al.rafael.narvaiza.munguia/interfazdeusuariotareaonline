**PMDM TO02**

**#Ejercicio 1**

Se realiza un conversor euro->dolar y dolar->euro.

Se presentan dos versiones, una con botón de convertir, otra, que una vez seleccionado uno de los conversores, realiza la conversión automática en el campo de texto de la moneda equivalente.
Para ello, se hace uso del método _onTextChangeListener_.


**#Ejercicio 2**

Se realiza un recyclerView con el objeto libro. Para ello se implementa el adaptador necesario, que se rellena con una colección que se obtiene de la clase LibroData. 

LibroData almacena un array de strings en el que se definen todo lo necesario para el objeto libro.
En este ejercicio concreto, solo se va a "levantar" una tarjeta con título, foto, año de publicación y autor.
Para setear el holder de la imagen, se utiliza la clase Glide, que previamente se ha tenido que implementar en las dependencias del build.gradle del módulo a través de:

`implementation 'com.github.bumptech.glide:glide:4.9.0'`

Para que Glide pueda acceder a las fotos, se ha de permitir que la actividad tenga acceso a internet. Para ello es necesario añadir permisos a AndroidManifest.xml

`<uses-permission android:name="android.permission.INTERNET"/>
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>`
     
**#Ejercicio 3**
Basándonos en la actividad anterior, se procede a trabajar sobre fragments.
En este caso, a la hora de implementar la muestra de imagenes en el adaptador, utilizo `setImageResource` pasando el id de R.drawable.

Toda la información del libro, se realiza a través de un seteo por parámetros del constructor libro1. Esta solución accesoria es debido a que perdí mucho tiempo dentro del fragment intentando levantar la imagen desde una url con Glide.
Para poder mostrar toda la información de la sinopsis, se ha añadido al layout del detalle un _scrollview_. De esta forma el layout no constriñe el espacio de visualización y permite deslizar para continuar leyendo la sinopsis.