package com.example.interfazdeusuariotareaonline.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;

import com.example.interfazdeusuariotareaonline.Ejercicio1.MainActivity2;
import com.example.interfazdeusuariotareaonline.Ejercicio2.MainActivity3;
import com.example.interfazdeusuariotareaonline.Ejercicio3.MainActivity4;
import com.example.interfazdeusuariotareaonline.R;
import com.example.interfazdeusuariotareaonline.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {


    private ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        Button buttonEJ1 = findViewById(R.id.buttonEjercicio1);
        buttonEJ1.setOnClickListener(v -> {
                Intent intent = new Intent(this, MainActivity2.class);
                startActivity(intent);
        });


        Button buttonEJ2 = findViewById(R.id.buttonEjercicio2);
        buttonEJ2.setOnClickListener(v -> {
            Intent intent = new Intent(this, MainActivity3.class);
            startActivity(intent);
        });


        Button buttonEJ3 = findViewById(R.id.buttonEjercicio3);
        buttonEJ3.setOnClickListener(v -> {
            Intent intent = new Intent(this, MainActivity4.class
            );
            startActivity(intent);
        });

    }
}