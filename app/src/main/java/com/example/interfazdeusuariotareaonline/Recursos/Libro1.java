
package com.example.interfazdeusuariotareaonline.Recursos;


/**
 *  @author Rafa Narvaiza
 *  PMDM TO 02
 *
 */



/**
 * This method is designed only for test on Android studio AVD Manager.
 * The unique diference between Libro class constructor and Libro1 class constructor is that Libro1 has an int parameter to get access to the R.drawable.ID.
 * With this ID, then I build throw parameters the Arraylist of objects Libro1.
 */


import android.os.Parcel;
import android.os.Parcelable;


public class Libro1 implements Parcelable {

    public Libro1(String titulo, int foto, String autor, String fechaPublicacion, String editorial, String Sinopsis) {
        this.titulo = titulo;
        this.autor = autor;
        this.fechaPublicacion = fechaPublicacion;
        this.foto = foto;
        this.editorial = editorial;
        this.Sinopsis = Sinopsis;
    }


    private String titulo;
    private String autor;
    private String fechaPublicacion;
    private String editorial;
    private String Sinopsis;
    private int foto;

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(String fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public Libro1() {
    }

    protected Libro1(Parcel in) {
        titulo = in.readString();
        autor = in.readString();
        fechaPublicacion = in.readString();
        foto = in.readInt();
    }

    public static final Creator<Libro1> CREATOR = new Creator<Libro1>() {
        @Override
        public Libro1 createFromParcel(Parcel in) {
            return new Libro1(in);
        }

        @Override
        public Libro1[] newArray(int size) {
            return new Libro1[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(titulo);
        dest.writeString(autor);
        dest.writeString(fechaPublicacion);
        dest.writeInt(foto);
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public String getSinopsis() {
        return Sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        Sinopsis = sinopsis;
    }
}

