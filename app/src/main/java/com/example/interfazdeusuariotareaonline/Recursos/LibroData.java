package com.example.interfazdeusuariotareaonline.Recursos;

/**
 *  @author Rafa Narvaiza
 *  PMDM TO 02
 *
 */

import java.util.ArrayList;
import java.util.List;

public class LibroData {



    // array to store data that will be displayed on.

    public static String [][] data = new String[][]{
            {"Castigo",
                    "http://i.ibb.co/MhcGmP7/castigo.jpg",
                    "Ferdinand Von Schirach",
                    "Narrativa salamandra",
                    "2018",
                    "Basada en casos reales, Crímenes, la primera obra de Ferdinand von Schirach, ganadora del prestigioso Premio Kleist y uno de los mayores éxitos de los últimos años en Alemania, fascinó y conmovió por la honestidad y lucidez con que planteaba la búsqueda de la verdad en los procesos criminales. Culpa, una colección de punzantes miniaturas sobre el insondable comportamiento humano, mereció de nuevo el elogio de la crítica y los lectores. Ahora, Von Schirach vuelve a convertir doce casos de su dilatada trayectoria profesional en sendas piezas de orfebrería literaria que tratan, con su aguzado instinto narrativo y su particular sentido del humor, las consecuencias penales y morales del castigo."
                    },
            {"El caso Collini",
                    "http://i.ibb.co/8b6fJvV/colini.jpg",
                    "Ferdinand Von Schirach",
                    "Narrativa salamandra",
                    "2011",
                    "El caso Collini, que narra una historia criminal de una concisión y una desnudez sobrecogedoras, suscitó un encendido debate al revelar la existencia de grandes deficiencias de la justicia en Alemania."
                    },
            {"Crimenes",
                    "http://i.ibb.co/5K7V3pM/crimenes.jpg",
                    "Ferdinand Von Schirach",
                    "Narrativa salamandra",
                    "2009",
                    "Crímenes plantea el fascinante tema de la escurridiza verdad en los procesos criminales y reflexiona sobre el sentido del castigo."
                    },
            {"Culpa",
                    "http://i.ibb.co/yRrTX22/culpa.jpg",
                    "Ferdinand Von Schirach",
                    "Narrativa salamandra",
                    "2010",
                    "Tras el éxito de Crímenes, Von Schirach vuelve a presentarnos una colección de punzantes miniaturas sobre el insondable comportamiento humano." ,
                    },
            {"Tabú",
                    "http://i.ibb.co/02s8kBn/tabu.jpg",
                    "Ferdinand Von Schirach",
                    "Narrativa salamandra",
                    "2013",
                    "Von Schirach vuelve a desplegar su mirada penetrante y sensible a las turbulencias internas del ser humano y su preocupación por las grandes cuestiones éticas de nuestro tiempo."
                    },
            {"Terror",
                    "http://i.ibb.co/TBwGX92/terror.jpg",
                    "Ferdinand Von Schirach",
                    "Narrativa salamandra",
                    "2015",
                    "Representada en casi un centenar de teatros en más de veinte países, donde ha cosechado un éxito inaudito, Terror aborda sin tapujos el abrumador problema ético al que se enfrenta el ser humano en situaciones límite, invitándonos a una reflexión profunda sobre el valor y el precio de la libertad.",
                    },

    };


    //Arraylist to store Libro object and filled with the strings contained on the array of Strings.


    public static List<Libro> getListData(){
        Libro libro;
        List<Libro> list = new ArrayList<>();

        for (String[] mData : data) {
            libro = new Libro();
            libro.setTitulo(mData[0]);
            libro.setFoto(mData[1]);
            libro.setAutor(mData[2]);
            libro.setEditorial(mData[3]);
            libro.setFechaPublicacion(mData[4]);
            libro.setSinopsis(mData[5]);

            list.add(libro);
        }
        return list;
    }

}
