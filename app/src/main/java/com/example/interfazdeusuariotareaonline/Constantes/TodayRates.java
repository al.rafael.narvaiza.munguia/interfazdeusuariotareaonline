package com.example.interfazdeusuariotareaonline.Constantes;


/**
 * @author Rafa Narvaiza
 * PMDM TO 02
 */

public class TodayRates {

    public static final double EURO_VALUE_ON_DOLAR = 1.21;
    public static final double DOLAR_VALUE_ON_EURO = 0.83;
}
