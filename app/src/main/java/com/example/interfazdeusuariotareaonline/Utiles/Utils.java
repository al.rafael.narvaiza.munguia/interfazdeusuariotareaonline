package com.example.interfazdeusuariotareaonline.Utiles;

import com.example.interfazdeusuariotareaonline.Constantes.TodayRates;

/**
 *  @author Rafa Narvaiza
 *  PMDM TO 02
 *
 */

public class Utils {

    TodayRates todayrates = new TodayRates();
    private double euroAmount;
    private double dolarAmount;

    public String fromDolarToEuro()throws NumberFormatException{
        double euro;
        String ConvertedEuro;
        euro = getDolarAmount()*todayrates.EURO_VALUE_ON_DOLAR;
        ConvertedEuro = Double.toString(euro);
        return ConvertedEuro;
    }

    public String fromEuroToDollar()throws NumberFormatException{
        double dollar;
        String ConvertedDollar;
        dollar = getEuroAmount()*todayrates.DOLAR_VALUE_ON_EURO;
        ConvertedDollar = Double.toString(dollar);
        return ConvertedDollar;
    }

    public double getEuroAmount() {
        return euroAmount;
    }

    public void setEuroAmount(double euroAmount) {
        this.euroAmount = euroAmount;
    }

    public double getDolarAmount() {
        return dolarAmount;
    }

    public void setDolarAmount(double dolarAmount) {
        this.dolarAmount = dolarAmount;
    }
}
