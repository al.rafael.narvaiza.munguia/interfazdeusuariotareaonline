package com.example.interfazdeusuariotareaonline.Ejercicio2;

/**
 * @author Rafa Narvaiza
 * PMDM TO 02
 */
/**
 * If your emulator display without issues the images, don't consider this comment.
 * On this activity we need to comment line 36 in order to disable LibroData and use Libro1 class to give the constructor through parameters the info.
 *
 */

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import com.example.interfazdeusuariotareaonline.R;
//import com.example.interfazdeusuariotareaonline.Ejercicio2.models.Libro1;
import com.example.interfazdeusuariotareaonline.Recursos.Libro;
import com.example.interfazdeusuariotareaonline.Recursos.LibroData;

import java.util.ArrayList;

public class MainActivity3 extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ArrayList<Libro> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);


        list = new ArrayList<>();
        list.addAll(LibroData.getListData());
        /*list.add(new Libro1("Castigo",R.drawable.castigo, "Ferdinand Von Schirach", "2018"));
        list.add(new Libro1("El caso Collini",R.drawable.colini, "Ferdinand Von Schirach", "2011"));
        list.add(new Libro1("Crimenes",R.drawable.crimenes, "Ferdinand Von Schirach", "2009"));
        list.add(new Libro1("Culpa",R.drawable.culpa, "Ferdinand Von Schirach", "2010"));
        list.add(new Libro1("Tabú",R.drawable.tabu, "Ferdinand Von Schirach", "2013"));
        list.add(new Libro1("Terror",R.drawable.terror, "Ferdinand Von Schirach", "2015"));*/

        showRecyclerViewList();

    }

    private void showRecyclerViewList() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ItemListAdapter listAdapter = new ItemListAdapter(this, list);
        listAdapter.setListLibros(list);
        recyclerView.setAdapter(listAdapter);
    }
}