package com.example.interfazdeusuariotareaonline.Ejercicio1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.interfazdeusuariotareaonline.Constantes.Constants_ES;
import com.example.interfazdeusuariotareaonline.R;
import com.example.interfazdeusuariotareaonline.Utiles.Utils;


public class MainActivity2WithOutImprove extends AppCompatActivity {

    Utils utils = new Utils();
    TextView tv1;
    TextView tv2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

    }


    public void onRadioButtonFromEuroToDollarClicked(View view){
        try{
            boolean checked = ((RadioButton)view).isChecked();
            tv1 = (TextView) findViewById(R.id.editTextInputDolar);
            tv2 = (TextView) findViewById(R.id.editTextInputEuro);

            switch (view.getId()){
                case R.id.radioButtonEuroToDolar:
                    if (checked){
                        utils.setEuroAmount((Double.parseDouble(tv2.getText().toString())));
                        tv1.setText(utils.fromEuroToDollar());
                    }
                    else
                        tv1.setText("0.0");
            }
        } catch (NumberFormatException ime ){
            Toast.makeText(this, Constants_ES.MESSAGE_ERROR_FOR_EURO, Toast.LENGTH_SHORT).show();
        }

    }

    public void onRadioButtonFromDollarToEuroClicked(View view){
        try{
            boolean checked = ((RadioButton)view).isChecked();
            tv1 = (TextView) findViewById(R.id.editTextInputDolar);
            tv2 = (TextView) findViewById(R.id.editTextInputEuro);

            switch (view.getId()){
                case R.id.radioButtonDolarToEuro:
                    if (checked){
                        utils.setEuroAmount((Double.parseDouble(tv1.getText().toString())));
                        tv2.setText(utils.fromEuroToDollar());
                    }
                    else
                        tv2.setText("0.0");
            }
        } catch (NumberFormatException ime ){
            Toast.makeText(this, Constants_ES.MESSAGE_ERROR_FOR_EURO, Toast.LENGTH_SHORT).show();
        }

    }
}