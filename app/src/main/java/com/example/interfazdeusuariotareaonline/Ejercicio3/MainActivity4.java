package com.example.interfazdeusuariotareaonline.Ejercicio3;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.example.interfazdeusuariotareaonline.R;
import com.example.interfazdeusuariotareaonline.Recursos.Libro;
import com.example.interfazdeusuariotareaonline.Recursos.Libro1;

/**
 *  @author Rafa Narvaiza
 *  PMDM TO 02
 * Deploying both fragments.
 */


public class MainActivity4 extends AppCompatActivity implements LibroFragment.onLibroInteractionListener{

    private LibroDetailFragment libroDetailFragment;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        FragmentManager fm=getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container_fragment,new LibroFragment());
        if(getSupportFragmentManager().getFragments().isEmpty()){
            ft.commit();
        }

    }

    @Override
    public void onLibroView(Libro1 libro) {
        FragmentManager fm=getSupportFragmentManager();
        libroDetailFragment = (LibroDetailFragment) fm.findFragmentByTag(LibroDetailFragment.TAG);

        if(libroDetailFragment == null){
            Bundle bundle = null;
            if(libro != null){
                bundle=new Bundle();
                bundle.putParcelable(Libro.TAG, libro);
            }
            libroDetailFragment =(LibroDetailFragment) LibroDetailFragment.newInstance(bundle);
        }

        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container_fragment, libroDetailFragment, LibroFragment.TAG);
        ft.addToBackStack(null);
        ft.commit();

    }


}