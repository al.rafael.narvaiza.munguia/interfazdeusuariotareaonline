package com.example.interfazdeusuariotareaonline.Ejercicio3;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.interfazdeusuariotareaonline.Activities.MainActivity;
import com.example.interfazdeusuariotareaonline.Ejercicio2.MainActivity3;
import com.example.interfazdeusuariotareaonline.R;
import com.example.interfazdeusuariotareaonline.Recursos.Libro1;
import java.util.List;

/**
 *  @author Rafa Narvaiza
 *  PMDM TO 02
 * Adapater for the cardview.
 */
public class MyLibroRecyclerViewAdapter extends RecyclerView.Adapter<MyLibroRecyclerViewAdapter.ViewHolder> {

    public interface OnItemClickListener{
        void onItemClick(Libro1 libro);
    }

    public List<Libro1> listaLibros;
    private OnItemClickListener listener;





    public List<Libro1> getListaLibros() {
        return listaLibros;
    }


    private void startActivity(Intent intent) {
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = listaLibros.get(position);
        holder.tvTitulo.setText(listaLibros.get(position).getTitulo());
        holder.imgList.setImageResource(listaLibros.get(position).getFoto());
        holder.bind(listaLibros.get(position), listener);

    }

    @Override
    public int getItemCount() {
        return listaLibros.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitulo;
        ImageView imgList;
        Libro1 mItem;
        LinearLayout linearLayout;

        public ViewHolder(final View view) {
            super(view);
            tvTitulo = itemView.findViewById(R.id.tituloLibro);
            imgList = itemView.findViewById(R.id.img_list);
            linearLayout = itemView.findViewById(R.id.linear_layout_card);

        }
        public void bind(final Libro1 libro, final OnItemClickListener listener){
            linearLayout.setOnClickListener((view -> {
                listener.onItemClick(libro);
            }));
        }

    }

    public MyLibroRecyclerViewAdapter(List<Libro1> listaLibros, OnItemClickListener listener) {
        this.listaLibros = listaLibros;
        this.listener = listener;
    }
}