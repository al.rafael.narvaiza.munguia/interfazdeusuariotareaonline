package com.example.interfazdeusuariotareaonline.Ejercicio3;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.interfazdeusuariotareaonline.R;
import com.example.interfazdeusuariotareaonline.Recursos.Libro;
import com.example.interfazdeusuariotareaonline.Recursos.Libro1;


/**
 /**
 * @author Rafa Narvaiza
 * PMDM TO 02

 * Here we show the fragment containing the details of each book.
 */
public class LibroDetailFragment extends Fragment {

    public static final String TAG = "libroDetalleFragment";
    TextView autor;
    TextView titulo;
    TextView fechaPublicacion;
    TextView editorial;
    TextView sinopsis;
    ImageView foto;

    public static Fragment newInstance(Bundle bundle){
        LibroDetailFragment fragment = new LibroDetailFragment();
        if(bundle != null ){
            fragment.setArguments(bundle);
        }

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_detail_book, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        autor = view.findViewById(R.id.autorDetail);
        titulo = view.findViewById(R.id.tituloDetail);
        fechaPublicacion = view.findViewById(R.id.fechaPubDetail);
        editorial = view.findViewById(R.id.editorialDetail);
        sinopsis = view.findViewById(R.id.sinopsisDetail);
        foto = view.findViewById(R.id.imagePortadaDetail);
        if(getArguments() != null ){
            Libro1 libro = getArguments().getParcelable(Libro.TAG);
            autor.setText(libro.getAutor());
            titulo.setText(libro.getTitulo());
            fechaPublicacion.setText(libro.getFechaPublicacion());
            editorial.setText(libro.getEditorial());
            sinopsis.setText(libro.getSinopsis());
            foto.setImageResource(libro.getFoto());

        }
    }
}