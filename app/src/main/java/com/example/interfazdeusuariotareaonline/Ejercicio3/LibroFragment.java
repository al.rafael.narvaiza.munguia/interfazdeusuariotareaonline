package com.example.interfazdeusuariotareaonline.Ejercicio3;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.interfazdeusuariotareaonline.R;
import com.example.interfazdeusuariotareaonline.Recursos.Libro1;
import java.util.ArrayList;

/**
 *  @author Rafa Narvaiza
 *  PMDM TO 02
 * Here we show the cardview and input data for the constructor Libro1
 */


public class LibroFragment extends Fragment {


    interface onLibroInteractionListener {
        void onLibroView(Libro1 libro);
    }


    public static final String TAG = "LibroFragment";

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private MyLibroRecyclerViewAdapter.OnItemClickListener listener;
    private onLibroInteractionListener callback;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_item_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ArrayList<Libro1> librosList = new ArrayList<>();
        super.onViewCreated(view, savedInstanceState);
        librosList.add(new Libro1("Castigo", R.drawable.castigo, "Ferdinand Von Schirach", "2018","Narrativa salamandra", "Basada en casos reales, Crímenes, la primera obra de Ferdinand von Schirach, ganadora del prestigioso Premio Kleist y uno de los mayores éxitos de los últimos años en Alemania, fascinó y conmovió por la honestidad y lucidez con que planteaba la búsqueda de la verdad en los procesos criminales. \n \n Culpa, una colección de punzantes miniaturas sobre el insondable comportamiento humano, mereció de nuevo el elogio de la crítica y los lectores. Ahora, Von Schirach vuelve a convertir doce casos de su dilatada trayectoria profesional en sendas piezas de orfebrería literaria que tratan, con su aguzado instinto narrativo y su particular sentido del humor, las consecuencias penales y morales del castigo."));
        librosList.add(new Libro1("El caso Collini",R.drawable.colini, "Ferdinand Von Schirach", "2011","Narrativa salamandra", "Primera novela de Ferdinand von Schirach -conocido por sus extraordinarios volúmenes de relatos Crímenes y Culpa-, El caso Collini suscitó un encendido debate sobre el funcionamiento de la justicia en Alemania y el libro, como sus dos anteriores, ocupó los primeros puestos en las listas de ventas en su país. \n\n Empleado diligente, de una discreción modélica, Fabrizio Collini trabajó como operario durante treinta y cuatro años en la Mercedes-Benz. Pero un día, ya jubilado, acude al legendario Hotel Adlon de Berlín, a dos pasos de la puerta de Brandenburgo, y asesina a sangre fría a un hombre anciano, sin motivo aparente. La defensa de Collini recae de oficio en el joven e inexperto abogado CasparLeinen, y lo que al principio parece una oportunidad para su incipiente carrera se convierte en un acuciante dilema profesional, ya que la víctima, un conocido y respetado empresario, es el abuelo de su primer amor, quien a su vez reaparece después de tantos años para pedirle que renuncie al caso. \n\n Así pues, Leinen no sólo se ve obligado a defender a un hombre que renuncia a defenderse y se niega a revelar el motivo del crimen, sino también debe resolver el conflicto de intereses. Su reputación y su carrera están en juego, más aún cuando el hallazgo de una pista apunta a un inquietante capítulo de la historia de la justicia alemana."));
        librosList.add(new Libro1("Crimenes",R.drawable.crimenes, "Ferdinand Von Schirach", "2009","Narrativa salamandra", "Esta serie de relatos basados en la experiencia profesional del autor, un reputado jurista alemán, se convirtió en uno de los mayores éxitos editoriales de los últimos años en Alemania. Además de obtener el prestigioso Premio Kleist, Crímenes mereció un torrente de elogiosos comentarios de la crítica y ocupó durante casi un año las listas de libros más vendidos. \n\n Cerca de setecientos casos desde que inició su carrera de abogado penalista en Berlín son el bagaje de vivencias que Von Schirach ha transformado, con un aguzado instinto narrativo, en una obra literaria de atmósfera cautivadora. \n\n El lenguaje sobrio y conciso de la búsqueda de la verdad judicial subraya la atención que Schirach fija en los crímenes cometidos por individuos corrientes, dejando que los hechos expongan la realidad con toda su crudeza. \n\n Profundamente original, revelador y lleno de matices, Crímenes nos habla con proximidad del ser humano, de su miseria y también de su grandeza."));
        librosList.add(new Libro1("Culpa",R.drawable.culpa, "Ferdinand Von Schirach", "2010","Narrativa salamandra", "Basada en casos reales, la primera obra literaria del jurista alemán Ferdinand von Schirach, Crímenes, fascinó y conmovió a los lectores y la crítica por la honestidad y lucidez con que planteaba el espinoso tema de la búsqueda de la verdad en los procesos criminales. Ahora, en esta nueva obra, el autor ha volcado quince relatos espigados de los más de 700 casos en los que ha participado a lo largo de su carrera.\n\n Con una sensibilidad especial para incidir en los detalles reveladores y una prosa depurada y precisa, Schirach narra quince punzantes historias. En unas, constatamos con angustia que quienes han perpetrado un crimen no son declarados culpables; en otras, que el sentimiento de culpa actúa con mayor celeridad que la ley; pero en todas ellas el lectorhallará los destellos de una honda inteligencia moral y un sigiloso, pero devastador, sentido del humor."));
        librosList.add(new Libro1("Tabú",R.drawable.tabu, "Ferdinand Von Schirach", "2013","Narrativa salamandra", "En esta novela Von Schirach replantea algunos de los grandes interrogantes de siempre: la validez del sistema de justicia en casos excepcionales, con la consiguiente justificación de transgredir o no la ley vigente.\n\n Nacido en una familia aristocrática venida a menos, Sebastian von Eschburg es un niño solitario e introvertido, con una madre que sólo se interesa por las carreras de caballos y un padre alcoholizado y aficionado a la caza, a quien, no obstante, lo une un fuerte vínculo. Con el tiempo, la extraordinaria percepción del color que posee Sebastian transformará al niño sensible y vulnerable en un famoso fotógrafo, un artista internacionalmente reconocido que plasma en sus obras una tormentosa relación entre ficción y realidad, verdad e ilusión. Sin embargo, en un giro inesperado, su vida cambia por completo cuando una llamada telefónica a la policía lo convierte de la noche a la mañana en el presunto asesino de una joven desaparecida."));
        librosList.add(new Libro1("Terror",R.drawable.terror, "Ferdinand Von Schirach", "2015","Narrativa salamandra", "Cada vez que se produce un atentado terrorista en un país occidental, provocando un sentimiento general de pánico y estupor en la sociedad, surge inevitablemente un dilema difícil de resolver: ¿estamos dispuestos a sacrificar nuestra libertad individual en aras de la seguridad colectiva? ¿Cuál es el camino correcto para preservar la democracia? \n\n Como oficial de la Fuerza Aérea de Alemania, Lars Koch debe intervenir en una situación de emergencia: un terrorista ha secuestrado un avión de Lufthansa y pretende estrellarlo contra el Allianz Arena de Múnich, donde en ese momento setenta mil espectadores asisten a un partido de fútbol internacional entre las selecciones de Alemania e Inglaterra. Contraviniendo las órdenes de sus superiores, y consciente de la responsabilidad que deberá asumir por su terrible acto, Koch derriba el aparato para impedir la masacre en el estadio, causando la muerte de las ciento sesenta y cuatro personas que viajaban a bordo. \n\n Así pues, el juicio al que se somete al infortunado piloto es el núcleo de la primera obra teatral del célebre abogado criminalista alemán Ferdinand von Schirach. Con una trama sencilla pero contundente, el autor de superventas como Crímenes y Culpa nos conmina a tomar partido como miembros del jurado popular que deberá dictar una sentencia de tintes dramáticos y consecuencias inquietantes."));

        recyclerView = view.findViewById(R.id.recycler_view_list_relative_layout);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        listener = libro -> callback.onLibroView(libro);

        adapter = new MyLibroRecyclerViewAdapter(librosList, listener);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            callback = (onLibroInteractionListener) context;
        }catch (ClassCastException e ){
            throw new ClassCastException(context.toString() + "onLibroInteractionListener must be called.");
        }
    }

}